<?xml version="1.0" encoding="UTF-8"?>

<!--
SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>

SPDX-License-Identifier: GPL-3.0-or-later
-->

<!DOCTYPE node PUBLIC
	"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"
	"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd" >
<node>
	<!--
		fr.romainvigier.zap.Zaps1:
		@short_description: Zap management interface

		Add, update and remove Zaps. Play a Zap or stop playing.

		This documents version 1 of the interface.
	-->
	<interface name="fr.romainvigier.zap.Zaps">
		<property name="version" type="u" access="read"/>
		<!--
			GetZaps:

			An array of all the Zaps. See the fr.romainvigier.zap.Zaps.GetZap() method for details of the Zap dictionary.
		-->
		<method name="GetZaps">
			<arg name="zap" type="aa{sv}" direction="out"/>
		</method>
		<!--
			GetZap:
			@uuid: UUID of the Zap to retrieve.
			@zap: Retrieved Zap.

			Retrieves the Zap with the UUID @uuid. The returned @zap is a dictionary with the following keys:

			<variablelist>
				<varlistentry>
					<term>s uuid</term>
					<listitem><para>UUID of the Zap, wrapped in a string variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>s name</term>
					<listitem><para>Name of the Zap, wrapped in a string variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>s collectionUuid</term>
					<listitem><para>UUID of the collection the Zap belongs to, a string variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>s color</term>
					<listitem>
						<para>
							Color of the Zap, wrapped in a string variant.
							<variablelist>
								<title>Accepted values</title>
								<varlistentry><term>gray</term><listitem>Gray</listitem></varlistentry>
								<varlistentry><term>red</term><listitem>Red</listitem></varlistentry>
								<varlistentry><term>orange</term><listitem>Orange</listitem></varlistentry>
								<varlistentry><term>green</term><listitem>Green</listitem></varlistentry>
								<varlistentry><term>blue</term><listitem>Blue</listitem></varlistentry>
								<varlistentry><term>purple</term><listitem>Purple</listitem></varlistentry>
							</variablelist>
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>b loop</term>
					<listitem><para>If the Zap is looping, wrapped in a boolean variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>d volume</term>
					<listitem><para>Volume of the Zap, wrapped in a double variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>b playing</term>
					<listitem><para>If the Zap is playing, wrapped in a boolean variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>d progress</term>
					<listitem><para>Playing progress of the Zap, wrapped in a double variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>u position</term>
					<listitem><para>Position of the Zap in its collection, wrapped in an unsigned integer variant</para></listitem>
				</varlistentry>
			</variablelist>
		-->
		<method name="GetZap">
			<arg name="uuid" type="s" direction="in"/>
			<arg name="zap" type="a{sv}" direction="out"/>
		</method>
		<!--
			AddZap:
			@name: Name of the Zap.
			@collectionUuid: UUID of the collection the Zap belongs to.
			@uri: URI of the audio file the Zap will play.
			@options: Dictionary of additional options.
			@zap: Added Zap.

			Add a Zap with the given @name, @collectionUuid, @uri and @options, and returns the created Zap. See the fr.romainvigier.zap.Zaps.GetZap() method for details on the returned @zap dictionary.

			<variablelist>
				<title>Available options</title>
				<varlistentry>
					<term>s color</term>
					<listitem><para>Color of the Zap, wrapped in a string variant. See the fr.romainvigier.zap.Zaps.GetZap() method for accepted values.</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>b loop</term>
					<listitem><para>If the Zap is looping, wrapped in a boolean variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>d volume</term>
					<listitem><para>Volume of the Zap, wrapped in a double variant</para></listitem>
				</varlistentry>
			</variablelist>


		-->
		<method name="AddZap">
			<arg name="name" type="s" direction="in"/>
			<arg name="collectionUuid" type="s" direction="in"/>
			<arg name="uri" type="s" direction="in"/>
			<arg name="options" type="a{sv}" direction="in"/>
			<arg name="zap" type="a{sv}" direction="out"/>
		</method>
		<!--
			RemoveZap:
			@uuid: UUID of the Zap to remove.

			Remove the Zap with UUID given in @uuid.
		-->
		<method name="RemoveZap">
			<arg name="uuid" type="s" direction="in"/>
		</method>
		<!--
			UpdateZap:
			@uuid: UUID of the Zap to update.
			@properties: Dictionary of properties to update.

			Update the Zap with the UUID @uuid. @properties is a dictionary that can have the following keys:

			<variablelist>
				<varlistentry>
					<term>name</term>
					<listitem><para>Name of the Zap, wrapped in a string variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>collectionUuid</term>
					<listitem><para>Collection UUID the Zap belongs to, wrapped in a string variant.</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>color</term>
					<listitem><para>Color of the Zap, wrapped in a string variant. See the fr.romainvigier.zap.Zaps.GetZap() method for accepted values.</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>loop</term>
					<listitem><para>If the Zap is looping, wrapped in a boolean variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>volume</term>
					<listitem><para>Volume of the Zap, wrapped in a double variant</para></listitem>
				</varlistentry>
				<varlistentry>
					<term>position</term>
					<listitem><para>Position of the Zap in its collection, wrapped in an integer variant</para></listitem>
				</varlistentry>
			</variablelist>
		-->
		<method name="UpdateZap">
			<arg name="uuid" type="s" direction="in"/>
			<arg name="properties" type="a{sv}" direction="in"/>
		</method>
		<!--
			PlayZap:
			@uuid: UUID of the Zap to play.

			Play the Zap with UUID given in @uuid.
		-->
		<method name="PlayZap">
			<arg name="uuid" type="s" direction="in"/>
		</method>
		<!--
			Stop:

			Stop playing any Zap.
		-->
		<method name="Stop"/>
		<!--
			FadeOut:
			@length: Length of the fade out, in seconds.

			Fade out and stop playing any Zap.
		-->
		<method name="FadeOut">
			<arg name="length" type="d" direction="in"/>
		</method>
		<!--
			ZapAdded:
			@uuid: UUID of the added Zap.

			Emitted when a Zap is added.
		-->
		<signal name="ZapAdded">
			<arg name="uuid" type="s"/>
		</signal>
		<!--
			ZapRemoved:
			@uuid: UUID of the removed Zap.

			Emitted when a Zap is removed.
		-->
		<signal name="ZapRemoved">
			<arg name="uuid" type="s"/>
		</signal>
		<!--
			ZapUpdated:
			@uuid: UUID of the updated Zap.

			Emitted when a Zap is updated.
		-->
		<signal name="ZapUpdated">
			<arg name="uuid" type="s"/>
		</signal>
	</interface>
</node>
